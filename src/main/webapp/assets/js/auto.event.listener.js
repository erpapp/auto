function autoEventListener(warp,allEvent) {
	var warp=$(warp);
	//界面美化组件
	this.initUIExt=function(){
		this.initDatetimepicker();
		$("textarea:not(.auto-editor)").autoTextarea();
		$("textarea.auto-editor").css("min-height",160).wysihtml5({locale: "zh-CN",html:true,shortcuts:{}});
	};
	//日期选择控件
	this.initDatetimepicker=function(){
		_handler(warp);
		_handler(warp.find("input[datetimepicker]"));
		function _handler(eles){
			eles.each(function(){
				var $this=$(this);
				if(isReadonly($this))return;
				var format=$this.attr("datetimepicker");
				if(typeof(format)=="undefined")return;
				if(_.isEmpty(format)){
					format="yyyy-mm-dd";
				}
				$this.prop("readonly",true);
				var minView=null;
				if(format.length<=10){
					minView=2;
				}else if(format.indexOf("hh")==0){
					minView=0;
				}
				var pickerPosition="bottom-left";
				var offset = $this.offset();
				var windowHeight = $(window).height();
				if((offset.top+100)>windowHeight) {
					pickerPosition="top-left";
				}
				var $div=$("<div id='date_wrap_"+$this.attr("id").replace(".","_")+"' class='input-group date'></div>");
				$this.wrap($div)
				$this.after("<span class='input-group-addon'><span class='glyphicon glyphicon-remove'></span></span><span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>");
				$this.parent().datetimepicker({format:format,minView:minView,clearBtn:false,autoclose: true,showMeridian: true,todayBtn: true,todayHighlight:true,pickerPosition:pickerPosition,language:"zh-CN"})
				.on('hide',function(e) {  
					resetValidateField($this);
	            });
	        });
		}
	};
	//添加下拉选择事件
	this.initInputSelect=function(){
		var listenerCls="auto-select-listener";
		_handler(warp);
		_handler(warp.find("input.auto-select,input.auto-tree-select"));
		function _handler(eles){
			eles.each(function(){
				var $this=$(this);
				if(!$this.hasClass("auto-select")&&!$this.hasClass("auto-tree-select"))return;
				if($this.hasClass(listenerCls))return;
				$this.addClass(listenerCls);
				var idIput=$this;
				var tempNodeId=$this.data("node-id");
				var tempNodeName=$this.data("node-name");
				var txtInput=null;
				if(!_.isUndefined(tempNodeName)){
					txtInput=jid(tempNodeName);
				}else{
					txtInput=$this.clone().attr({id:$this.attr("id")+"_txt",name:"",type:"text"}).val(trim($this.data("text")));
					$this.after(txtInput);
				}
				if(!_.isUndefined(tempNodeId)){
					idIput=jid(tempNodeId);
				}else{
					idIput.hide();
				}
				
				txtInput.click(function(){
					if(isReadonly($this))return;
					txtInput.prop("readonly",true);
					var uri=$this.data("uri");
            		var onCheck=getFunction($this.data("onckeck"));
            		var onClick=getFunction($this.data("onclick"));
            		var nodeId=idIput.attr("id");
            		var nodeName=txtInput.attr("id");
            		var multi=$this.data("select-multi") ? true : false;
            		
	            	if(txtInput.hasClass("auto-tree-select")){//树形显示
	            		txtInput.zTreeSelect({uri:uri,nodeId:nodeId,nodeName:nodeName,multi:multi,onCheck:onCheck,onClick:onClick});
	            	}else{
	            		
	            	}
	            });
	        });
		}
	};
	//添加验证事件
	this.initFormValidator=function(){
		var cls="bv-form";
		_handler(warp);
		_handler(warp.find("form."+cls));
		function _handler(eles){
			eles.each(function(){
				var $this=$(this);
				if(this.tagName.toLowerCase()!="form"||!$this.hasClass(cls))return;
				$this.attr("data-bv-message","表单数据验证失败").bootstrapValidator();
			});
		}
	};
	//按钮事件
	this.initBtnEvent=function(){
		var cls="auto-btn";
		var listenerCls="auto-btn-listener";
		_handler(warp);
		_handler(warp.find("."+cls));
		function _handler(eles){
			eles.each(function(){
				var $this=$(this);
				if(isReadonly($this))return;
				if(!$this.hasClass(cls)||$this.hasClass(listenerCls))return;
				$this.addClass(listenerCls);
				$this.click(function(){
	            	if($this.hasClass("auto-submit")||$this.hasClass("auto-submit-ajax")){
	            		var formId=trim($this.data("form"));
	            		var $form = null;
	            		if(formId!=""){
	            			$form=$("#"+formId);
	            		}
	            		if(null==$form||$form.length<1){
	            			$form = $this.parents("form:eq(0)");
	            		}
	            		if(null==$form||$form.length<1){
	            			return;
	            		}
	            		var interceptor=getFunction($this.data("interceptor"));
	            		if(null!=interceptor){
	            			if(!interceptor.call(this,$form)){
	            				showMsg("表单提交失败","error");
	            				return;
	            			}
	            		}
	            		var bootstrapValidator = $form.data("bootstrapValidator");
	            		if(null!=bootstrapValidator){
	            			bootstrapValidator.validate();
	            			if(!bootstrapValidator.isValid()){
	            				showMsg("表单验证失败","error");
	            				return;
	            			}
	            		}
	            		waitLoading(true);
	            		if($this.hasClass("auto-submit-ajax")){
	            			var datas = $form.serializeArray();
	            			var uri = $form.attr("action");
	            			postData(uri, datas, null, getFunction($this.data("callback")));
	            		}else{
	            			if(null!=bootstrapValidator){
	            				bootstrapValidator.defaultSubmit(); 
	            			}else{
	            				$form.submit();
	            			}
	            		}
	            	}else {
	            		var uri=$this.data("uri");
	            		var callback=getFunction($this.data("callback"));
	            		if($this.hasClass("auto-load")){
		            		var targetId=trim($this.data("target"),"worker");
		            		loadUri(targetId,uri,callback);
		            	}else if($this.hasClass("auto-dialog")){
		            		loadUriDialog($this.data("title"), uri, null, $this.data("width"), $this.data("hight") , null, callback);
		            	}
	            	}
	            });
	        });
		}
	};
	//添加文件管理
	this.initAutoFile=function(){
		var cls="auto-file";
		var listenerCls="auto-file-listener";
		_handler(warp);
		_handler(warp.find("input."+cls));
		function _handler(eles){
			eles.each(function(){
				var $this=$(this);
				if(this.tagName.toLowerCase()!="input"||!$this.hasClass(cls)||$this.hasClass(listenerCls))return;
				$this.addClass(listenerCls);
				$this.autoFile();
			});
		}
	};
	//初始化所有事件
	this.initAll=function(){
		this.initUIExt();
		this.initBtnEvent();
		this.initInputSelect();
		this.initFormValidator();
		this.initAutoFile();
	};
	
	if(_.isBoolean(allEvent)?allEvent:false){
		this.initAll();
	}else{
		this.initUIExt();
	}
};
