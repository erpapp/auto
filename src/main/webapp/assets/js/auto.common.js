var dictCache={}
function dict_name(code,val){
	var dictList = $.data(dictCache,code);
	if(null == dictList){
		$.ajax({url:"dict/cache/"+code,dataType:"json",type:"GET",async:false,
			success:function(data){
				if(data.success)$.data(dictCache, code, data.data);
		}});
		dictList = $.data(dictCache,code);
	}
	var name="";
	for(var i=0;null!=dictList&&i<dictList.length;i++){
		var d=dictList[i];
		if(d.val==val){
			name=d.name;
			break;
		}
	}
	return name;
};

function _addSysFeedback(){
	var buttons = [ {
		label : '提交',
		cssClass : 'btn-success btn-sm',
		icon : 'fa fa-check-square',
		action : function(dialog) {
			if(jid("model.userName").val()==""){
				jid("model.userName").focus();
				showMsg("您的姓名不能为空","error");
				return;
			}
			if(jid("model.title").val()==""){
				jid("model.title").focus();
				showMsg("反馈主题不能为空","error");
				return;
			}
			postValidatorForm("auto-data-form", function(data) {
				if (data.success) {
					closeDialog();
				}
				showMsg(data.msg,data.success?"info":"error");
			});
		}
	}, {
		label : '取消',
		cssClass : 'btn-info btn-sm',
		icon : 'fa fa-window-close',
		action : function(dialog) {
			dialog.close();
		}
	} ];
	loadUriDialog("系统反馈", "index/feedback", null, 600, 500, buttons);
}