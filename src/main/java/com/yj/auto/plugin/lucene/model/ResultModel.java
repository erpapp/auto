package com.yj.auto.plugin.lucene.model;

import java.util.ArrayList;
import java.util.List;

import com.yj.auto.plugin.lucene.utils.LuceneUtil;

public class ResultModel {
	private Integer page = 1;// 当前页
	private Integer previousPage;// 上一页
	private Integer nextPage;// 下一页
	private List<IndexModel> list = new ArrayList<IndexModel>();// 查询结果

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPreviousPage() {
		if (page > 1) {
			previousPage = page - 1;
		}
		return previousPage;
	}

	public Integer getNextPage() {
		if (list.size() >= LuceneUtil.LUCENE_PAGE_SIZE) {
			nextPage = page + 1;
		}
		return nextPage;
	}

	public List<IndexModel> getList() {
		return list;
	}

	public void addData(IndexModel model) {
		this.list.add(model);
	}

}
