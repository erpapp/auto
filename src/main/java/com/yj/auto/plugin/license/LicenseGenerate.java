package com.yj.auto.plugin.license;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Scanner;

public class LicenseGenerate {
	public static void main(String[] args) throws Exception {
		System.out.println("---序列号生成系统启动---");
		System.out.println("");
		Scanner sc = new Scanner(System.in);
		System.out.print("请输入服务器的MAC地址：");
		String mac = sc.nextLine();
		System.out.println("");
		if (null == mac || mac.trim().length() < 1) {
			System.out.println("---MAC地址为空，序列号生成失败---");
		} else {
			mac = mac.trim();
			System.out.println("---正在生成序列号---");
			AutoLicense lic = LicenseTools.getLic(null, mac);
			File file = new File("license.dat");
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(lic.getText().getBytes());
			fos.close();
			System.out.println("");
			System.out.println("序列号生成成功:" + file.getAbsolutePath());
			System.out.println("");
			System.out.println("序列号验证:" + lic.verify(mac));
		}
		sc.close();
		System.out.println("");
	}
}
