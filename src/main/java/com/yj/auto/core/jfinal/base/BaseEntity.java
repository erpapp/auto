package com.yj.auto.core.jfinal.base;

import java.util.List;

import com.jfinal.json.FastJson;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IBean;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.SqlPara;

public abstract class BaseEntity<M extends Model<M>> extends Model<M> implements IBean {

	/**
	 * 获取数据表名称
	 * 
	 * @return
	 */
	public abstract String getTableName();

	/**
	 * 获取数据表主键名称
	 * 
	 * @return
	 */
	public abstract String getTablePK();

	/**
	 * 获取数据表主键名称
	 * 
	 * @return
	 */
	public abstract String getTableRemark();

	// 根据字段条件，批量删除
	public boolean deleteByColumn(String column, Integer... ids) {
		Kv cond = Kv.by("array", ids).set("table", getTableName()).set("column", column);
		SqlPara sql = Db.getSqlPara("base.delete.array", cond);
		return Db.update(sql) > 0;
	}

	// 根据字段条件，批量查询
	public List<M> findByColumn(String column, Integer... ids) {
		Kv cond = Kv.by("array", ids).set("table", getTableName()).set("column", column);
		SqlPara sql = Db.getSqlPara("base.find.array", cond);
		return this.find(sql);
	}

	public String toJson() {
		return FastJson.getJson().toJson(this);
	}
}
