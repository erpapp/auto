package com.yj.auto.core.jfinal.context;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

import com.jfinal.core.JFinalFilter;
import com.yj.auto.Constants;
import com.yj.auto.utils.ClassUtil;

public class AutoFilter extends JFinalFilter {

	public void init(FilterConfig filterConfig) throws ServletException {
		System.setProperty("autoLogPath", com.yj.auto.Constants.SYS_LOG_PATH);
		PropertyConfigurator.configure(System.getProperties());
		DOMConfigurator.configure(ClassUtil.getPathByClassPath(Constants.CONF_LOG4J));
		super.init(filterConfig);
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		super.doFilter(req, res, chain);
	}

	public void destroy() {
		super.destroy();
	}
}
