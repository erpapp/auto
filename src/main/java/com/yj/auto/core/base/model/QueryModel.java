package com.yj.auto.core.base.model;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.yj.auto.Constants;

/**
 * jfinal的动态SQL调用方法com.jfinal.plugin.activerecord.Db.getSqlPara(String,
 * Map)方法，第二个参数必须为Map
 */
public class QueryModel extends Kv {
	private Integer page = 1;// 页码,从1开始
	private Integer size = Constants.PAGE_SIZE;// 分页大小
	// 以下为默认查询条件
	private String keyword;
	private String state;
	private String type;
	private Integer id;
	private Integer userId;
	private Integer orgId;
	private String stime;
	private String etime;
	private String key01;
	private String key02;
	private String key03;
	private Object key04;
	private Object key05;
	private String orderby;// 排序关键字,可以为多个列,如: username desc,user_type desc

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		if (!StrKit.isBlank(keyword)) {
			this.set("keyword", keyword);
			this.keyword = keyword;
		}
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		if (!StrKit.isBlank(state)) {
			this.set("state", state);
			this.state = state;
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		if (!StrKit.isBlank(type)) {
			this.set("type", type);
			this.type = type;
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		if (null != id) {
			this.set("id", id);
			this.id = id;
		}
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		if (null != userId) {
			this.set("userId", userId);
			this.userId = userId;
		}
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		if (null != orgId) {
			this.set("orgId", orgId);
			this.orgId = orgId;
		}
	}

	public String getStime() {
		return stime;
	}

	public void setStime(String stime) {
		if (!StrKit.isBlank(stime)) {
			this.set("stime", stime);
			this.stime = stime;
		}
	}

	public String getEtime() {
		return etime;
	}

	public void setEtime(String etime) {
		if (!StrKit.isBlank(etime)) {
			this.set("etime", etime);
			this.etime = etime;
		}
	}

	public String getKey01() {
		return key01;
	}

	public void setKey01(String key01) {
		if (!StrKit.isBlank(key01)) {
			this.set("key01", key01);
			this.key01 = key01;
		}
	}

	public String getKey02() {
		return key02;
	}

	public void setKey02(String key02) {
		if (!StrKit.isBlank(key02)) {
			this.set("key02", key02);
			this.key02 = key02;
		}
	}

	public String getKey03() {
		return key03;
	}

	public void setKey03(String key03) {
		if (!StrKit.isBlank(key03)) {
			this.set("key03", key03);
			this.key03 = key03;
		}
	}

	public Object getKey04() {
		return key04;
	}

	public void setKey04(Object key04) {
		if (null != key04) {
			this.set("key04", key04);
			this.key04 = key04;
		}
	}

	public Object getKey05() {
		return key05;
	}

	public void setKey05(Object key05) {
		if (null != key04) {
			this.set("key05", key05);
			this.key05 = key05;
		}
	}

	public String getOrderby() {
		return orderby;
	}

	public void setOrderby(String orderby) {
		if (!StrKit.isBlank(orderby)) {
			this.set("orderby", orderby);
			this.orderby = orderby;
		}
	}

}
