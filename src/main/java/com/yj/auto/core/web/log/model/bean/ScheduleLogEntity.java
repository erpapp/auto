package com.yj.auto.core.web.log.model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;
import com.yj.auto.core.jfinal.base.*;

/**
 *  任务执行日志
 */
@SuppressWarnings("serial")
public abstract class ScheduleLogEntity<M extends ScheduleLogEntity<M>> extends BaseEntity<M> {
	
	public static final String TABLE_NAME = "t_log_schedule"; //数据表名称
	
	public static final String TABLE_PK = "id"; //数据表主键
	
	public static final String TABLE_REMARK = "任务执行日志"; //数据表备注

	public String getTableName(){
		return TABLE_NAME;
	}

	public String getTableRemark(){
		return TABLE_REMARK;
	}
	
	public String getTablePK(){
		return TABLE_PK;
	}
	
   		
	/**
	 * Column ：id
	 * @return 主键
	 */
   		
	public Integer getId(){
   		return get("id");
   	}
	
	public void setId(Integer id){
   		set("id" , id);
   	}	
   		
	/**
	 * Column ：schedule_id
	 * @return 任务主键
	 */
   	@NotNull 	
	public Integer getScheduleId(){
   		return get("schedule_id");
   	}
	
	public void setScheduleId(Integer scheduleId){
   		set("schedule_id" , scheduleId);
   	}	
   		
	/**
	 * Column ：state
	 * @return 执行结果
	 */
   	@NotBlank
	@Length(max = 32)	
	public String getState(){
   		return get("state");
   	}
	
	public void setState(String state){
   		set("state" , state);
   	}	
   		
	/**
	 * Column ：server_ip
	 * @return 执行服务器主机名或者IP
	 */
   	@NotBlank
	@Length(max = 32)	
	public String getServerIp(){
   		return get("server_ip");
   	}
	
	public void setServerIp(String serverIp){
   		set("server_ip" , serverIp);
   	}	
   		
	/**
	 * Column ：stime
	 * @return 开始时间
	 */
   	@NotNull 	
	public Date getStime(){
   		return get("stime");
   	}
	
	public void setStime(Date stime){
   		set("stime" , stime);
   	}	
   		
	/**
	 * Column ：etime
	 * @return 结束时间
	 */
   	@NotNull 	
	public Date getEtime(){
   		return get("etime");
   	}
	
	public void setEtime(Date etime){
   		set("etime" , etime);
   	}	
   		
	/**
	 * Column ：spent
	 * @return 耗时（毫秒）
	 */
   	@NotNull 	
	public Long getSpent(){
   		return get("spent");
   	}
	
	public void setSpent(Long spent){
   		set("spent" , spent);
   	}	
   		
	/**
	 * Column ：remark
	 * @return 描述
	 */
   	@Length(max = 1024)	
	public String getRemark(){
   		return get("remark");
   	}
	
	public void setRemark(String remark){
   		set("remark" , remark);
   	}	
}