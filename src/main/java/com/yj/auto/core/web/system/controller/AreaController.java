package com.yj.auto.core.web.system.controller;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.yj.auto.Constants;
import com.yj.auto.core.base.annotation.Controller;
import com.yj.auto.core.base.annotation.Valid;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.base.model.ResponseModel;
import com.yj.auto.core.jfinal.base.BaseController;
import com.yj.auto.core.web.system.model.Area;
import com.yj.auto.core.web.system.model.Resource;
import com.yj.auto.core.web.system.service.AreaService;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.plugin.ztree.ZTreeNode;

/**
 * 区域管理 管理
 * 
 * 描述：
 * 
 */
@Controller(viewPath = "system")
public class AreaController extends BaseController {
	private static final Log logger = Log.getLog(AreaController.class);

	private static final String RESOURCE_URI = "area/index";
	private static final String INDEX_PAGE = "area_index.html";
	private static final String FORM_PAGE = "area_form.html";
	private static final String SHOW_PAGE = "area_show.html";

	AreaService areaSrv = null;

	public void index() {
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<Area> dt = getDataTable(query, areaSrv);
		renderJson(dt);
	}

	public void get() {
		Integer id = getParaToInt(0);
		Area model = areaSrv.get(id);
		setAttr("model", model);
		render(SHOW_PAGE);
	}

	public void form() {
		Integer id = getParaToInt(0);
		Area model = null;
		if (null != id && 0 != id) {
			model = areaSrv.get(id);
		} else {
			model = new Area();
			model.setHot("02");
		}
		if (null != model.getParentId()) {
			Area parent = areaSrv.get(model.getParentId());
			if (null != parent) {
				setAttr("parent", parent);
			}
		}
		setAttr("model", model);
		render(FORM_PAGE);
	}

	@Valid(type = Area.class)
	public boolean saveOrUpdate(Area model) {
		model.setLuser(getSuId());
		model.setLtime(Constants.NOW());
		boolean success = false;
		if (null == model.getId()) {
			success = areaSrv.save(model);
		} else {
			success = areaSrv.update(model);
		}
		ResponseModel<String> res = renderSaveOrUpdate(success, areaSrv);
		return res.isSuccess();
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(areaSrv);
		return res.isSuccess();
	}

	public void children() {
		Integer id = getParaToInt("id");
		if (null == id)
			id = new Integer(1);
		String state = getPara("state");
		List<Area> list = areaSrv.children(id, state);
		List<ZTreeNode> result = new ArrayList<ZTreeNode>();
		for (Area root : list) {
			List<Area> subs = areaSrv.children(root.getId(), state);
			ZTreeNode t = getZTreeNode(root, subs.size() > 0);
			if (null == id && t.getIsParent()) {
				t.setOpen(true);
				for (Area obj : subs) {
					ZTreeNode st = getZTreeNode(obj, areaSrv.children(obj.getId(), state).size() > 0);
					t.addChildren(st);
				}
			}
			result.add(t);
		}
		renderJson(result);
	}

	private ZTreeNode getZTreeNode(Area c, boolean parent) {
		ZTreeNode t = new ZTreeNode(c.getId().toString(), c.getName());
		t.setNocheck(false);
		t.setIsParent(parent);
		t.addAttribute("level", c.getLevel());
		return t;
	}
}