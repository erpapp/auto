package com.yj.auto.core.web.log.controller;

import com.jfinal.ext.kit.DateKit;
import com.jfinal.log.Log;
import com.yj.auto.core.base.annotation.Controller;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.base.model.ResponseModel;
import com.yj.auto.core.jfinal.base.BaseController;
import com.yj.auto.core.web.log.model.ReadingLog;
import com.yj.auto.core.web.log.service.ReadingLogService;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.utils.DateUtil;

/**
 * 阅读记录 管理
 * 
 * 描述：
 * 
 */
@Controller(viewPath = "log", key = "/log/reading")
public class ReadingLogController extends BaseController {
	private static final Log logger = Log.getLog(ReadingLogController.class);

	private static final String RESOURCE_URI = "log/reading/index";
	private static final String INDEX_PAGE = "reading_index.html";
	private static final String SHOW_PAGE = "reading_show.html";

	ReadingLogService readingLogSrv = null;

	public void index() {
		QueryModel query = new QueryModel();
		query.setStime(DateUtil.toDateStr(DateUtil.addDay(DateUtil.date(), -7)));
		setAttr("query", query);
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<ReadingLog> dt = getDataTable(query, readingLogSrv);
		renderJson(dt);
	}

	public void get() {
		Integer id = getParaToInt(0);
		ReadingLog model = readingLogSrv.get(id);
		setAttr("model", model);
		render(SHOW_PAGE);
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(readingLogSrv);
		return res.isSuccess();
	}

	// 根据条件清空日志
	public boolean clear() {
		String type = getPara("type");
		String keyword = getPara("keyword");
		String stime = getPara("stime");
		String etime = getPara("etime");
		boolean success = readingLogSrv.delete(type, keyword, stime, etime);
		ResponseModel<String> res = new ResponseModel<String>(success);
		res.setMsg("清空" + readingLogSrv.getDao().getTableRemark() + (res.isSuccess() ? "成功" : "失败"));
		renderJson(res);
		return success;
	}
}