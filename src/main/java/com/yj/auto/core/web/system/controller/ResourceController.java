package com.yj.auto.core.web.system.controller;

import java.io.UnsupportedEncodingException;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.jfinal.kit.*;
import com.jfinal.log.Log;
import com.yj.auto.Constants;
import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.base.model.*;
import com.yj.auto.core.jfinal.base.*;
import com.yj.auto.core.web.system.model.*;
import com.yj.auto.core.web.system.service.*;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.plugin.ztree.ZTreeNode;

/**
 * 系统资源 管理 描述：
 * 
 */
@Controller(viewPath = "system")
public class ResourceController extends BaseController {
	private static final Log logger = Log.getLog(ResourceController.class);

	private static final String RESOURCE_URI = "resource/index";
	private static final String INDEX_PAGE = "resource_index.html";
	private static final String FORM_PAGE = "resource_form.html";
	private static final String SHOW_PAGE = "resource_show.html";

	ResourceService resourceSrv = null;

	public void index() {
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<Resource> dt = getDataTable(query, resourceSrv);
		renderJson(dt);
	}

	public void children() {
		Integer id = getParaToInt("id");
		List<Resource> list = resourceSrv.children(id, "");
		List<ZTreeNode> result = new ArrayList<ZTreeNode>();
		for (Resource c : list) {
			List<Resource> subs = resourceSrv.children(c.getId(), "");
			ZTreeNode t = getZTreeNode(c, subs.size() > 0);
			if (null == id && t.getIsParent()) {
				t.setOpen(true);
				for (Resource s : subs) {
					t.addChildren(getZTreeNode(s, resourceSrv.children(s.getId(), "").size() > 0));
				}
			}
			result.add(t);
		}
		renderJson(result);
	}

	private ZTreeNode getZTreeNode(Resource c, boolean parent) {
		ZTreeNode t = new ZTreeNode(c.getId().toString(), c.getName());
		// if (StrKit.notBlank(c.getImg())) {
		// t.setIcon(c.getImg());
		// }
		t.setIsParent(parent);
		return t;
	}

	public boolean updateSort() {
		Integer[] id = getParaValuesToInt();
		boolean success = resourceSrv.updateSort(id);
		ResponseModel<String> res = new ResponseModel<String>(success);
		renderJson(res);
		return success;
	}

	public void form() throws Exception {
		String id = getPara(0);
		Resource model = null;
		if (id.startsWith("new_")) {
			model = new Resource();
			model.setParentId(getParaToInt(1));
			model.setSort(getParaToInt(2));
			model.setName(getParaDecode(3));
		} else {
			model = resourceSrv.get(Integer.parseInt(id));
		}
		if (null != model.getParentId()) {
			Resource parent = resourceSrv.get(model.getParentId());
			if (null != parent) {
				setAttr("parent", parent);
			}
		}
		setAttr("model", model);
		render(FORM_PAGE);

	}

	@Valid(type = Resource.class)
	public boolean saveOrUpdate(Resource model) {
		model.setLuser(getSuId());
		model.setLtime(Constants.NOW());
		List<ResourceAuthModel> items = this.getBeans(ResourceAuthModel.class, "items");
		model.setAuths(items);
		boolean success = false;
		if (null == model.getId()) {
			success = resourceSrv.save(model);
		} else {
			success = resourceSrv.update(model);
		}
		ResponseModel<String> res = new ResponseModel<String>(success);
		res.setData(model.getId().toString());
		res.setMsg("保存" + resourceSrv.getDao().getTableRemark() + (res.isSuccess() ? "成功" : "失败"));
		renderJson(res);
		return success;
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(resourceSrv);
		return res.isSuccess();
	}
}