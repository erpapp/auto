package com.yj.auto.core.web.system.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.system.model.bean.*;
/**
 * 区域管理
 */
@SuppressWarnings("serial")
@Table(name = Area.TABLE_NAME, key = Area.TABLE_PK, remark = Area.TABLE_REMARK)
public class Area extends AreaEntity<Area> {

}