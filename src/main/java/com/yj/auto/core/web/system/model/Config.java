package com.yj.auto.core.web.system.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.system.model.bean.*;
/**
 * 系统配置
 */
@SuppressWarnings("serial")
@Table(name = Config.TABLE_NAME, key = Config.TABLE_PK, remark = Config.TABLE_REMARK)
public class Config extends ConfigEntity<Config> {

}