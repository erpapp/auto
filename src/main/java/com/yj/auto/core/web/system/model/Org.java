package com.yj.auto.core.web.system.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.system.model.bean.*;
/**
 * 系统机构
 */
@SuppressWarnings("serial")
@Table(name = Org.TABLE_NAME, key = Org.TABLE_PK, remark = Org.TABLE_REMARK)
public class Org extends OrgEntity<Org> {
	
}