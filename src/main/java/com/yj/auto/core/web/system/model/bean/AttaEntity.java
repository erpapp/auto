package com.yj.auto.core.web.system.model.bean;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.yj.auto.core.jfinal.base.BaseEntity;

/**
 * 系统附件表
 */
@SuppressWarnings("serial")
public abstract class AttaEntity<M extends AttaEntity<M>> extends BaseEntity<M> {

	public static final String TABLE_NAME = "t_sys_atta"; // 数据表名称

	public static final String TABLE_PK = "id"; // 数据表主键

	public static final String TABLE_REMARK = "系统附件表"; // 数据表备注

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getTableRemark() {
		return TABLE_REMARK;
	}

	public String getTablePK() {
		return TABLE_PK;
	}

	/**
	 * Column ：id
	 * 
	 * @return 文件ID
	 */

	public Integer getId() {
		return get("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * Column ：name
	 * 
	 * @return 文件名
	 */
	@NotBlank
	@Length(max = 256)
	public String getName() {
		return get("name");
	}

	public void setName(String name) {
		set("name", name);
	}

	/**
	 * Column ：ext
	 * 
	 * @return 扩展名
	 */
	@Length(max = 32)
	public String getExt() {
		return get("ext");
	}

	public void setExt(String ext) {
		set("ext", ext);
	}

	/**
	 * Column ：size
	 * 
	 * @return 文件大小
	 */
	@NotNull
	public Long getSize() {
		return get("size");
	}

	public void setSize(Long size) {
		set("size", size);
	}

	/**
	 * Column ：path
	 * 
	 * @return 文件路径
	 */
	@NotBlank
	@Length(max = 256)
	public String getPath() {
		return get("path");
	}

	public void setPath(String path) {
		set("path", path);
	}

	/**
	 * Column ：mime_type
	 * 
	 * @return 文件类型
	 */
	@Length(max = 256)
	public String getMimeType() {
		return get("mime_type");
	}

	public void setMimeType(String mimeType) {
		set("mime_type", mimeType);
	}

	/**
	 * Column ：md5
	 * 
	 * @return 文件MD5
	 */
	@Length(max = 256)
	public String getMd5() {
		return get("md5");
	}

	public void setMd5(String md5) {
		set("md5", md5);
	}

	/**
	 * Column ：ctime
	 * 
	 * @return 创建时间
	 */
	@NotNull
	public Date getCtime() {
		return get("ctime");
	}

	public void setCtime(Date ctime) {
		set("ctime", ctime);
	}

	/**
	 * Column ：user_id
	 * 
	 * @return 创建人
	 */
	@NotNull
	public Integer getUserId() {
		return get("user_id");
	}

	public void setUserId(Integer userId) {
		set("user_id", userId);
	}

	/**
	 * Column ：data_type
	 * 
	 * @return 文件来源
	 */
	@Length(max = 32)
	public String getDataType() {
		return get("data_type");
	}

	public void setDataType(String dataType) {
		set("data_type", dataType);
	}

	/**
	 * Column ：data_id
	 * 
	 * @return 文件来源主键
	 */

	public Integer getDataId() {
		return get("data_id");
	}

	public void setDataId(Integer dataId) {
		set("data_id", dataId);
	}
}