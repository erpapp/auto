package com.yj.auto.component.base.controller;

import java.util.*;

import com.jfinal.kit.*;
import com.jfinal.log.Log;
import com.yj.auto.core.base.model.*;
import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.jfinal.base.*;
import com.yj.auto.plugin.table.model.DataTables;
import com.yj.auto.Constants;
import com.yj.auto.component.base.model.*;
import com.yj.auto.component.base.service.*;

/**
 * 知识库 管理
 * 
 * 描述：
 * 
 */
@Controller(viewPath = "component/base")
public class DocController extends BaseController {
	private static final Log logger = Log.getLog(DocController.class);

	private static final String RESOURCE_URI = "doc/index";
	private static final String INDEX_PAGE = "doc_index.html";
	private static final String FORM_PAGE = "doc_form.html";
	private static final String SHOW_PAGE = "doc_show.html";

	DocService docSrv = null;

	public void index() {
		render(INDEX_PAGE);
	}

	public void show() {
		setAttr("readonly", true);
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<Doc> dt = getDataTable(query, docSrv);
		renderJson(dt);
	}

	public void get() {
		Integer id = getParaToInt(0);
		Doc model = docSrv.get(id);
		setAttr("model", model);
		render(SHOW_PAGE);
	}

	public void form() {
		Integer id = getParaToInt(0);
		Doc model = null;
		if (null != id && 0 != id) {
			model = docSrv.get(id);
		} else {
			model = new Doc();
			model.setSort(Constants.MODEL_SORT_DEFAULT);
		}
		setAttr("model", model);
		render(FORM_PAGE);
	}

	@Valid(type = Doc.class)
	public boolean saveOrUpdate(Doc model) {
		model.setLuser(getSuId());
		model.setLtime(Constants.NOW());
		initAttaMap(model, DocService.ATTA_DOC);// 附件
		boolean success = false;
		if (null == model.getId()) {
			model.setUserId(model.getLuser());
			model.setCtime(model.getLtime());
			success = docSrv.save(model);
		} else {
			success = docSrv.update(model);
		}
		ResponseModel<String> res = renderSaveOrUpdate(success, docSrv);
		return res.isSuccess();
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(docSrv);
		return res.isSuccess();
	}
}