package com.yj.auto.component.base.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.component.base.model.bean.*;
/**
 * 通知公告
 */
@SuppressWarnings("serial")
@Table(name = Notice.TABLE_NAME, key = Notice.TABLE_PK, remark = Notice.TABLE_REMARK)
public class Notice extends NoticeEntity<Notice> {

}